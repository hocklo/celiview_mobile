package com.celiview.Models;

public class Product {
	
	private int Id;
	private String Name, Description, Image, Price;
	/**
	 * Constructor buit del model Producte
	 */
	public Product() {
		
	}
	/**
	 * Constructor Complet del model producte
	 * Amb ell es crea un producte.
	 * @param id
	 * @param Name
	 * @param Description
	 * @param Image
	 * @param Price
	 */
	public Product(int id, String Name,String Description,String Image,String Price){
		this.Id = id;
		this.Name = Name;
		this.Description = Description;
		this.Image = Image;
		this.Price = Price;
	}
	/**
	 * Products Getters & Setters
	 */
	
	/**
	 * 
	 * @return this.Id
	 */
	public int getId() {
		return Id;
	}
	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		Id = id;
	}
	/**
	 * 
	 * @return this.Name
	 */
	public String getName() {
		return Name;
	}
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		Name = name;
	}
	/**
	 * 
	 * @return this.Description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * 
	 * @param desription
	 */
	public void setDescription(String desription) {
		Description = desription;
	}
	/**
	 * 
	 * @return this.Image
	 */
	public String getImage() {
		return Image;
	}
	/**
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		Image = image;
	}
	/**
	 * 
	 * @return this.price
	 */
	public String getPrice() {
		return Price;
	}
	/**
	 * 
	 * @param price
	 */
	public void setPrice(String price) {
		Price = price;
	}
	
	
	
}
