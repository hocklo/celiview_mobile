package com.celiview.Models;

import java.io.Serializable;

public class Shop implements Serializable {
	private int id;
	private String name;
	private String phone;
	private String description;
	private String address;
	private String image;
	private double latitude;
	private double longitude;
	/**
	 * Constructor Shop amb tots els parametres 
	 * amb ell crearem una nova botiga.
	 * @param name
	 * @param phone
	 * @param description
	 * @param address
	 * @param latitude
	 * @param longitude
	 */
	public Shop(int id, String name, String phone, String description,
				String address, double latitude, double longitude) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.description = description;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	/**
	 * Constructor Shop vacio
	 */
	public Shop(){};
	
	/**
	 * GetId
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * SetId
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * GetName
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * SetName
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * GetPhone
	 * @return phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * SetPhone
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * GetDescription
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * SetDescription
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * GetAddress
	 * @return address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * SetAddress
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * GetLatitude
	 * @return latitude
	 */
	public double getLatitude() {
		return latitude;
	}
	/**
	 * SetLatitude
	 * @param latitude
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	/**
	 * GetLongitude
	 * @return longitude
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * SetLongitude
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	/**
	 * GetImage
	 * @return image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * SetImage
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}
	
	
}
