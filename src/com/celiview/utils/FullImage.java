package com.celiview.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.celiview.mapManager.R;
/**
 * Classe Product_full_image 
 * S'encarrega de mostrar el producte en tamany més gran
 * de tal manera que es puguin contemplar amb més qualitat
 * els detalls de la imatge.
 * @author hocklo
 * @extends Activity
 *
 */
public class FullImage extends Activity {
	private String url, title;
	private Bitmap myBitmap;
	@Override
	/**
	 * 
	 * Metode onCreate de la clase Product_full_image
	 * @param savedInstanceState @type Bundle
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_full_image);
		//final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final View contentView = findViewById(R.id.imageView1);
		
		Intent intent = getIntent();
		
		String type = intent.getStringExtra("type");
		if(type.equals("Shop")){
			url = "http://www.celiview.com/Cview/web/bundles/cview/img/shops/";
			title = "Tienda: ";
			myBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.shop_not_found);
		}else{
			url = "http://www.celiview.com/Cview/web/bundles/cview/img/products/";
			title = "Producto: ";
			myBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.product_not_found);
		}
		String name = intent.getStringExtra("name");
		String image = intent.getStringExtra("icon");
		String description = intent.getStringExtra("description");
		
		this.setTitle(title+name);
		
		ImageView iv = (ImageView) findViewById(R.id.imageView1);
		//iv.setImageDrawable(mIcons.getDrawable(position));
		URL newurl = null;
		try {
			newurl = new URL(url+image);
			//Toast.makeText(this, newurl.toString(), Toast.LENGTH_SHORT).show();
		} catch (MalformedURLException e) {
			System.out.println("Product_full_image:onCreate:MalformedURLException"+e);
		} 
		Bitmap tempImage = null;
		try {
			tempImage = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
		} catch (IOException e) {
			System.out.println("Product_full_image:onCreate:IOException"+e);
		}
		if(tempImage!=null){
			iv.setImageBitmap(tempImage);
		}else{
			iv.setImageBitmap(myBitmap);
		}
		
		TextView txtDescripcion = (TextView) findViewById(R.id.lblProductDescripcion);
		txtDescripcion.setText(description);
	}
}
