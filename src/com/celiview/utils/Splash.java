package com.celiview.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Window;
import android.widget.Toast;

import com.celiview.mapManager.MapView;
import com.celiview.mapManager.R;
/**
 * Es la primera pantalla que es carrega a l'aplicació 
 * sencarrega de mostrar el logo de l'aplicació com a 
 * metode de publicitat es purament decorativa.
 * @author hocklo
 * @extends Activity
 */
public class Splash extends Activity {

	 protected boolean active = true;
	 protected int splashTime = 1000;
	 //Shared preferences
	   private SharedPreferences cview_prefs;
	   private boolean firstTime;
	    
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.activity_splash);
	        
	        Thread splashThread = new Thread(){
	         @Override
	         public void run(){
	          try{
	           int waited = 0;
	           while(active && (waited < splashTime)){
	            sleep(3000);
	            if(active){
	             waited += 3000;
	            }else{
	            	break;
	            }
	            
	           }
	          } catch(InterruptedException e){
	           
	          } finally{
	           openApp();
	           //stop();
	          }
	         }
	        };
	        splashThread.start();
	    }
	    /**
	     * S'encarrega de finalitzar la view i cridar
	     * la view MapView que es el nucli de l'aplicació
	     * @name openApp
	     */
		private void openApp(){
	     finish();
	     //Shared Preferences 
			cview_prefs = getSharedPreferences("Celiview",Context.MODE_PRIVATE);
			firstTime = cview_prefs.getBoolean("firstTime", true);
			
			//if(firstTime){
				//Toast.makeText(this, "First Time", Toast.LENGTH_LONG).show();
			    //Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			    //startActivity(intent);
			//}else{
				startActivity(new Intent(this,MapView.class));
			//}
		}
	 
	}