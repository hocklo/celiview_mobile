package com.celiview.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.celiview.Models.Product;
import com.celiview.mapManager.R;

public class ImageAndTextAdapter extends ArrayAdapter<Product> {

	private LayoutInflater mInflater;
	private int mViewResourceId;
	private ArrayList<Product> shopProducts;
	/**
	 * La classe imageAndTextAdapter s'encarrega d'adaptar els diferents
	 * textos e imatges a l'adapter d'una listView d'aquesta manera 
	 * li pasem la feina amb aquesta clase i la list view s'oblida 
	 * de que te que fer aquesta part.
	 * @param ctx
	 * @param viewResourceId
	 * @param products
	 */
	public ImageAndTextAdapter(Context ctx, int viewResourceId,
			ArrayList<Product> products) {
		super(ctx, viewResourceId,products);
		
		mInflater = (LayoutInflater)ctx.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		
		mViewResourceId = viewResourceId;
		
		shopProducts = products;
	}

	@Override
	/**
	 * Metode getView , retorna un objecte View i rep com a parametre la posició
	 * de la llista la view i el ViewGroup
	 * Sencarrega d'inflar la posició de la llista corresponent.
	 */
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(mViewResourceId, null);
		
		ImageView iv = (ImageView)convertView.findViewById(R.id.list_image);

		URL newurl = null;
		try {
			newurl = new URL("http://www.celiview.com/Cview/web/bundles/cview/img/products/"+shopProducts.get(position).getImage());
			//Toast.makeText(getContext(), newurl.toString(), Toast.LENGTH_SHORT).show();
		} catch (MalformedURLException e) {
			System.out.println("ImageAndTextAdapter:View:getView:MalformedURL"+e);
		} 
		Bitmap tempImage = null;
		try {
			tempImage = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(tempImage!=null){
			iv.setImageBitmap(tempImage);
		}else{
			iv.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(),R.drawable.product_not_found));
		}
		TextView tvProduct = (TextView)convertView.findViewById(R.id.tvProduct);
		tvProduct.setText(shopProducts.get(position).getName());
		
		TextView tvDescription = (TextView)convertView.findViewById(R.id.tvProDescription);
		if(shopProducts.get(position).getDescription().length()>100){
			tvDescription.setText(shopProducts.get(position).getDescription().substring(0, 100)+"...");
		}else{
			tvDescription.setText(shopProducts.get(position).getDescription());
		}
		
		TextView tvPrice = (TextView)convertView.findViewById(R.id.tvPrice);
		tvPrice.setText(shopProducts.get(position).getPrice()+" €");
		
		return convertView;
	}
}
