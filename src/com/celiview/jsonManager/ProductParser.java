package com.celiview.jsonManager;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.celiview.Models.Product;
import com.celiview.Models.Shop;


public class ProductParser {
	// JSON Node names
	private final String TAG_PRODUCTS = "products";
	private final String TAG_ID = "id";
	private final String TAG_NAME = "Name";
	private final String TAG_DESCRIPTION = "Description";
	private final String TAG_IMAGE = "Image";
	private final String TAG_PRICE = "Price";
	
	//Contenedor de tiendas
	private ArrayList<Product> aProducts;
	
	//Shopss JSONArray
	private JSONArray products = null;
	
	public ProductParser(JSONObject json){		 		 
		try {
		    //Inicializamos el array de tiendas
		    products = json.getJSONArray(TAG_PRODUCTS);
		    aProducts = new ArrayList<Product>();
		    if(products.length()!=0){
			    //Recorremos las tiendas encontradas
			    for(int i = 0; i < products.length(); i++){
	
			        JSONObject tempObject = products.getJSONObject(i);
			        
			        Product product = new Product();
			        
			        //Guardamos los valores del JSON dentro de un array
			        product.setId(tempObject.getInt(TAG_ID));
			        product.setName(tempObject.getString(TAG_NAME));
			        product.setDescription(tempObject.getString(TAG_DESCRIPTION));
			        product.setImage(tempObject.getString(TAG_IMAGE));
			        product.setPrice(tempObject.getString(TAG_PRICE));
			         
			        aProducts.add(product);
			    }
		    }
		} catch (JSONException e) {
		    System.out.println("productParser:constructor:JSONException:"+e);
		}
	}
	/**
	 * GetAshops 
	 * Devuelve ArrayList de los productos que 
	 * tiene la tienda seleccionada en la activity
	 * anterior
	 * @return aShops
	 */
	public ArrayList<Product> getaProducts() {
		return aProducts;
	}
	
	
}
