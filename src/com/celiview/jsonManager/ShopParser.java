package com.celiview.jsonManager;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.celiview.Models.Shop;


public class ShopParser {
	// JSON Node names
	private final String TAG_SHOPS = "shops";
	private final String TAG_ID = "id";
	private final String TAG_NAME = "Name";
	private final String TAG_PHONE = "Phone";
	private final String TAG_DESCRIPTION = "Description";
	private final String TAG_ADDRESS = "Direction";
	private final String TAG_IMAGE = "Image";
	private final String TAG_LATITUDE = "Latitude";
	private final String TAG_LONGITUDE = "Longitude";
	
	//Contenedor de tiendas
	private ArrayList<Shop> aShops;
	
	//Shopss JSONArray
	private JSONArray shops = null;
	
	public ShopParser(JSONObject json){		 		 
		try {
		    //Inicializamos el array de tiendas
		    shops = json.getJSONArray(TAG_SHOPS);
		    aShops = new ArrayList<Shop>();
		    //Recorremos las tiendas encontradas
		    for(int i = 0; i < shops.length(); i++){

		        JSONObject tempObject = shops.getJSONObject(i);
		        
		        Shop shop = new Shop();
		        
		        //Guardamos los valores del JSON dentro de un array
		        shop.setId(tempObject.getInt(TAG_ID));
		        shop.setName(tempObject.getString(TAG_NAME));
		        shop.setPhone(tempObject.getString(TAG_PHONE));
		        shop.setDescription(tempObject.getString(TAG_DESCRIPTION));
		        shop.setAddress(tempObject.getString(TAG_ADDRESS));
		        shop.setImage(tempObject.getString(TAG_IMAGE));
		        shop.setLatitude(tempObject.getDouble(TAG_LATITUDE));
		        shop.setLongitude(tempObject.getDouble(TAG_LONGITUDE));
		         
		        aShops.add(shop);
		    }
		} catch (JSONException e) {
		    System.out.println("shopParser:constructor:JSONException:"+e);
		}
	}
	/**
	 * GetAshops 
	 * Devuelve ArrayList de Tiendas
	 * @return aShops
	 */
	public ArrayList<Shop> getaShops() {
		return aShops;
	}
	
	
}
