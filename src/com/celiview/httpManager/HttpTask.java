package com.celiview.httpManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
/**
 * La classe HttpTask s'encarregarà de gestiona totes 
 * les crides Http cap al servidor web .
 * D'aquesta manera aconseguim separa un modul que es 
 * repateix varies vegades en tota l'aplicació i 
 * modulitzem l'aplicació, per fer-la més efectiva
 * i més simple a la hora de mantenir-la.
 * @author hocklo
 *
 */
public class HttpTask{
	private HttpClient httpclient;
	private HttpResponse response;
	
	private String tempResult;
	/**
	 * El Constructor per defecte de HttpTask rep com a parametre
	 * una URL . 
	 * 
	 * A partir d'aquesta URL extreu la informació que es vol 
	 * aconseguir.
	 * 
	 * @param URL @type String
	 */
	public HttpTask(String URL){
		httpclient = new DefaultHttpClient();
		try {
			  response = httpclient.execute(new HttpGet(URL));
			  
			  StatusLine statusLine = response.getStatusLine();
			     
			  if(statusLine.getStatusCode()==HttpStatus.SC_OK){
				  
				  ByteArrayOutputStream out = new ByteArrayOutputStream();
				  response.getEntity().writeTo(out);
				  out.close();
				  String responseString = out.toString();
				  tempResult = responseString;
				  //Toast.makeText(MapView.this,responseString, Toast.LENGTH_LONG).show();
			  }else{
			  //Close the connection
				  response.getEntity().getContent().close();
				  //throw new IOException(statusLine.getReasonPhrase());
			  }
			  
		} catch (ClientProtocolException e) {
			  // TODO Auto-generated catch block
			 System.out.println("Exception:MapView:onLocationChanged:ClientProtocolException\n");
		} catch (IOException e) {
			 // TODO Auto-generated catch block
			 System.out.println("Exception:MapView:onLocationChanged:IOException\n");
		}
	}
	/**
	 * El metode getTempResult retorna la String
	 * que es troba guardada temporalment.
	 * @return
	 */
	public String getTempResult() {
		return tempResult;
	}
	/**
	 * set TempResult implementa el resultat
	 * no es recomenable ja que sempre el resultat
	 * tindriem que extreurel de la web per més 
	 * fiabilitat.
	 * @param tempResult
	 */
	public void setTempResult(String tempResult) {
		this.tempResult = tempResult;
	}
	
	
}