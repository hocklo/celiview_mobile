package com.celiview.fragmentManager;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.celiview.Models.Product;
import com.celiview.Models.Shop;
import com.celiview.httpManager.HttpTask;
import com.celiview.jsonManager.ProductParser;
import com.celiview.mapManager.R;
import com.celiview.utils.ImageAndTextAdapter;
import com.celiview.utils.FullImage;
/**
 * Aquesta Clase s'encarrega de carregar un Fragment,
 * Aquest fragment serà el de la llista de productes.
 * 
 * Lo que farà es cridar el listview, i posarli com 
 * a adapter l'xml R.id.list
 * 
 * Acte seguit rellenara la llista seguint l'adapter.
 * Duran l'acció de emplenar la llista recaptara la 
 * informació de les imatges via Http
 * 
 * @author hocklo
 * @category Fragments
 * @extends android.suppor.v4.app.Fragment
 *
 */
public class ProductFragment extends android.support.v4.app.Fragment{
	public static final String PRODUCTNAME = "product_name";
	private ArrayList<Product> aProducts;
	private Product product;
	private Shop shop;
	
	public ProductFragment(){
		
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_shop_lst_products,
				container, false);
		
		shop = (Shop) this.getArguments().getSerializable("shop");
		
		ListView listProducts = (ListView) rootView.findViewById(R.id.list);
		
		Context ctx = getActivity().getApplicationContext();
		Resources res = ctx.getResources();
		
		HttpTask tempHttpTask = new HttpTask("http://celiview.com/Cview/web/app_dev.php/api/products/shop/"+shop.getId()+"/");
		//GUARDAR EL RESULTADO EN UNA STRING
		String tempProductsResult = tempHttpTask.getTempResult();
		//COMPROVAR QUE LAS TIENDAS NO LLEGAN VACIAS
		if(tempProductsResult != null ){
		  //Toast.makeText(MapView.this, tempShopsResult,Toast.LENGTH_LONG).show();
		  
			try {
				JSONObject jObject = new JSONObject(tempProductsResult);
				//Inicializamos el Objeto ShopParser
				ProductParser productParser = new ProductParser(jObject);
				//Recogemos el array de tiendas
				aProducts = productParser.getaProducts();
			} catch (JSONException e) {
				System.out.println("MapView:onLocationChanged:JSONException:"+e);
				//MOSTRAR UN ALERT DE AVISO
				//Toast.makeText(getActivity().getApplicationContext(),"JSONException:"+ tempProductsResult,Toast.LENGTH_LONG).show();
			}
		}else{
			//MOSTRAR ERROR
			//Toast.makeText(getActivity().getApplicationContext(),"No hay tiendas en tu zona.", Toast.LENGTH_SHORT).show();  
		}
		if(aProducts != null){
			listProducts.setAdapter(new ImageAndTextAdapter(ctx, R.layout.list_row, aProducts));
				// Click event for single list row
			listProducts.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
						Intent i = new Intent();
						i.setClass(getActivity().getApplicationContext(), FullImage.class);
						i.putExtra("type","Product");
						i.putExtra("name",aProducts.get(position).getName());
						i.putExtra("icon", aProducts.get(position).getImage());
						i.putExtra("description", aProducts.get(position).getDescription());
						// start the sample activity
						startActivity(i);
					}
				});
		}
			return rootView;
		}
}