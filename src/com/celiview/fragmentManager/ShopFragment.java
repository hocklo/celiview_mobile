package com.celiview.fragmentManager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.celiview.Models.Shop;
import com.celiview.mapManager.R;
import com.celiview.utils.FullImage;
/**
 * La classe ShopFragment s'encarrega d'omplir el fragment
 * que conte la informació de botiga per fer aixo , 
 * se li enviara via intent un objecte serialiable que segueix
 * el model Shop.
 * 
 * Acte seguit la clase Shop Fragment construira la vista 
 * rellenan les dades.
 * @author hocklo
 * @category Fragments
 * @extends android.suppor.v4.app.Fragment
 */
public class ShopFragment extends android.support.v4.app.Fragment{
	private Shop shop;
	
	public ShopFragment(){
		
	}
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_shop_info, 
				container, false);
		
		shop = (Shop) this.getArguments().getSerializable("shop");
		
		ImageView iv = (ImageView)rootView.findViewById(R.id.shopImage);

		URL newurl = null;
		try {
			newurl = new URL("http://www.celiview.com/Cview/web/bundles/cview/img/shops/"+shop.getImage());
			//Toast.makeText(getContext(), newurl.toString(), Toast.LENGTH_SHORT).show();
		} catch (MalformedURLException e) {
			System.out.println("ImageAndTextAdapter:View:getView:MalformedURL"+e);
		} 
		Bitmap tempImage = null;
		try {
			tempImage = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(tempImage!=null){
			iv.setImageBitmap(tempImage);
		}else{
			iv.setImageBitmap(BitmapFactory.decodeResource(this.getResources(),R.drawable.shop_not_found));
			iv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent();
					i.setClass(getActivity().getApplicationContext(), FullImage.class);
					i.putExtra("type","Shop");
					i.putExtra("name",shop.getName());
					i.putExtra("icon", shop.getImage());
					i.putExtra("description", shop.getDescription());
					// start the FullImage Activity
					startActivity(i);
					
				}
			});
		}
		
		TextView tvPhone = (TextView) rootView
				.findViewById(R.id.tvTelefono);
		tvPhone.setText(shop.getPhone());
		
		TextView tvAddress = (TextView) rootView
				.findViewById(R.id.tvDireccion);
		
		tvAddress.setText(shop.getAddress());
		
		TextView tvDescription = (TextView) rootView
				.findViewById(R.id.tvDescripcion);
		
		tvDescription.setText(shop.getDescription());
		
		Button btnStreetView = (Button) rootView.findViewById(R.id.btnStreetView);
		btnStreetView.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent streetViewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse
				      ("google.streetview:cbll="+shop.getLatitude()+","+shop.getLongitude()+"&cbp=13,198.24,,0,-8.19&mz=7"));
				         
				startActivity(streetViewIntent);
			}
		});
		return rootView;
	}
}