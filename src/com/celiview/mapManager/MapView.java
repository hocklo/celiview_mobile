package com.celiview.mapManager;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.celiview.Models.Shop;
import com.celiview.httpManager.HttpTask;
import com.celiview.info.ShopDetails;
import com.celiview.jsonManager.ShopParser;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
/**
 * Aplicacio de Projecte Celiview
 * Clase Principal --> MapView
 *                 --> Extends --> android.support.v4.app.FragmentActivity
 * 
 * Al arrancar la aplicació un cop finalizada la splashView
 * es llança aquesta clase la "MapView".
 * Aquesta clase conte tots els metodes que fan referencia tant a 
 * mapes com a gelocalització i sera ella la unica encarregada de gestionarlos.
 * 		
 * 
 * @author hocklo
 * @category maps
 * @extends android.support.v4.app.FragmentActivity
 */
@SuppressLint("NewApi")
/**
 * Al arrancar la aplicació un cop finalizada la splashView
 * es llança aquesta clase la "MapView".
 * Aquesta clase conte tots els metodes que fan referencia tant a 
 * mapes com a gelocalització i sera ella la unica encarregada de gestionarlos.
 * @author hocklo
 * @implements OnMarkerClickListener
 * @implements OnMyLocationChangeListener
 */
public class MapView extends android.support.v4.app.FragmentActivity implements OnMarkerClickListener, OnMyLocationChangeListener{
	final Context context = this;
	private static final int CONTROL_MOVE = 3600;
	private static final int ZOOM = 12;
	private int move;
	private GoogleMap mapa = null;
	private LocationManager locationManager;
	protected HttpResponse response; 
	private ArrayList<Shop> aShops;
	private HashMap<Marker, Shop> aMarkers;
	private boolean enabledGPS;
	private double  lastKnownLatitude;
    private double  lastKnownLongitude;
    private int zoom ;
    private Dialog dialog;
    private boolean initMyLocation;
    
    
	@Override
	/**
	 * Metode On Create 
	 * @param savedInstanceState @type Bundle
	 */
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initMyLocation = true;
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
	    //Adquirim la ref. al system Location Manager
		  locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		//Preparem la comprovacio del GPS
	      enabledGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	      
	      
	      
		  // Check if enabled and if not send user to the GSP settings
		  // Better solution would be to display a dialog and suggesting to 
		  // go to the settings
		  if (!enabledGPS) {
		      Toast.makeText(this, "GPS signal not found", Toast.LENGTH_LONG).show();
		      Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		      startActivity(intent);
		  }else{
				setUpMap();  
		  }
	}//FINAL ON CREATE
	/**
	 * Prepara el mapa l'instancia en la ultima posicion
	 * coneguda.
	 * 
	 */
	private void setUpMap() {
		mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMap();
      
        mapa.getUiSettings().setAllGesturesEnabled(true);
	    mapa.getUiSettings().setZoomControlsEnabled(true);
	    mapa.setOnMarkerClickListener(this);
	    mapa.setMyLocationEnabled(true);
	    mapa.setOnMyLocationChangeListener(this);
	}
	private void setUpLocation(Location location){
		LatLng initLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mapa.addMarker(new MarkerOptions()
          		.position(initLocation)
          		.title("Last Location Known.")
          		.icon(BitmapDescriptorFactory.fromResource(R.drawable.wait_clock)));
        CameraUpdate Center = CameraUpdateFactory.newLatLng(initLocation);	
           
	    CameraUpdate Zoom = CameraUpdateFactory.zoomTo(ZOOM);
	    
	    mapa.moveCamera(Center);
	    mapa.animateCamera(Zoom);
	    initMyLocation = false;
	}
	/**
	 * Prepara l'aplicació per a ser pausada
	 * Tambe deshabilita el locationManager
	 * i deshabilita l'enabledGPS
	 */
	protected void onPause(){
		if(locationManager != null){
			//Adquirim la ref. al system Location Manager
			  locationManager = null;
			//Preparem la comprovacio del GPS
		      enabledGPS = false;
		      initMyLocation = true;
		}
		super.onPause();
	}
	/**
	 * Es cridara quan es recargui la classe MapView.class
	 * o quan es torni enrrere des de una altre view.
	 * Reestableix el mapa, ja que en el metode onPause
	 * les deshabilitem.
	 * 
	 */
	protected void onResume(){
		if(locationManager == null){
			//Adquirim la ref. al system Location Manager
			  locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			//Preparem la comprovacio del GPS
		      enabledGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		    //setUp map needed
			  if(enabledGPS){
				setUpMap();
			  }
		}
		super.onResume();
	}
	/**
	 * Si la version es mas pequeña que HONYCOMB usar el metodo deprecated
	 * @param mContext
	 * @return width
	 */
	public static int getWidth(Context mContext){
	    int width=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB){                   
	        Point size = new Point();
	        display.getSize(size);
	        width = size.x;
	    }
	    else{
	        width = display.getWidth();  // deprecated
	    }
	    return width;
	}
	/**
	 * Si la version es mas pequeña que HONYCOMB usar el metodo deprecated
	 * @param mContext
	 * @return height
	 */
	public static int getHeight(Context mContext){
	    int height=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB){               
	        Point size = new Point();
	        display.getSize(size);
	        height = size.y;
	    }else{          
	        height = display.getHeight();  // deprecated
	    }
	    return height;      
	}
	@Override
	/**
	 * OnCreateOptionsMenu --> prepara el menu
	 * @param menu @type Menu
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	/**
	 * onOptionsItemSelected es cridara quan es premi
	 * una opció del menu.
	 * @param item @type MenuItem
	 */
	public boolean onOptionsItemSelected(MenuItem item){	
		switch(item.getItemId()){
			case R.id.action_about:
				// custom dialog
				dialog = new Dialog(context);
				dialog.setContentView(R.layout.customdialog);
				dialog.setTitle("About Celiview");

				Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	/**
	 * onMarkerClick rebrà com a parametre el marker
	 * que aura pulsat l'usuari
	 * 
	 * Si el marker coincideix amb algun marker del nostre
	 * HashMap llançarem la Activity de detalls de botiga
	 * i li pasarem la botiga clicada via Intent
	 * 
	 * @param pressMarker @type Marker
	 */
	public boolean onMarkerClick(Marker pressMarker) {
		System.out.println("CLICK MARKER");
			if(aMarkers.containsKey(pressMarker)){
				Intent i = new Intent(this, ShopDetails.class);
				Shop shop = aMarkers.get(pressMarker);
				i.putExtra("shop", shop);
		        startActivity(i);
			}
		return false;
	}
	/**
	 * Metodo onMyLocationChange, cuando la posicion del usuario
	 * se mueva o simplemente canvie se activara.
	 * @param location
	 */
	@Override
	public void onMyLocationChange(Location location) {
		// Es crida quan es troba una nova localitzacio a traves del network provider.
	      //location = new Location(LocationManager.GPS_PROVIDER);
		if(initMyLocation){
	      	setUpLocation(location);
      	}
		if(move == 0){
	      Projection proj  = mapa.getProjection();
		  
		  Point daltEsquerra = new Point(0,0);
		  LatLng de_coord = proj.fromScreenLocation(daltEsquerra);
		  
		  Point baixDreta = new Point(getWidth(getApplicationContext()),getHeight(getApplicationContext()));
		  LatLng ba_coord = proj.fromScreenLocation(baixDreta);
		  //System.out.println("baixDreta:"+ba_coord.latitude+" "+ba_coord.longitude);
		  // Cridem a la clase httpTask per fer la crida de la url que conte el nostre json
		  HttpTask tempHttpTask = new HttpTask("http://celiview.com/Cview/web/app_dev.php/api/shop/"+
			  		"latitude/"+de_coord.latitude+"/"+ba_coord.latitude+"/"+ //LONGITUD DALT ESQUERRA / BAIX DRETA
			  		"longitude/"+ba_coord.longitude+"/"+de_coord.longitude+"/"); //LATITUD DALT ESQUERRA / BAIX DRETA
		  //GUARDAR EL RESULTADO EN UNA STRING
		  String tempShopsResult = tempHttpTask.getTempResult();
		  //COMPROVAR QUE LAS TIENDAS NO LLEGAN VACIAS
		  if(tempShopsResult != null ){			  
			try {
				JSONObject jObject = new JSONObject(tempShopsResult);
				//Inicializamos el Objeto ShopParser
				ShopParser shopParser = new ShopParser(jObject);
				//Recogemos el array de tiendas
				aShops = shopParser.getaShops();
				//aMarkers = new ArrayList<Marker>();
				if(aMarkers == null){
					aMarkers = new HashMap<Marker, Shop>();
				}
				mapa.clear();
				for(Shop shop : aShops){
					Marker marker = mapa.addMarker(new MarkerOptions()
					        .position(new LatLng(shop.getLatitude(), shop.getLongitude()))
					        .title(shop.getName())
							.icon(BitmapDescriptorFactory.fromResource(R.drawable.shop_marker))
							);
					aMarkers.put(marker, shop);
				}
				
			} catch (JSONException e) {
				System.out.println("MapView:onLocationChanged:JSONException:"+e);
				//MOSTRAR UN ALERT DE AVISO
				  Toast.makeText(MapView.this,"JSONException:"+ tempShopsResult,Toast.LENGTH_LONG).show();
			}
			//MOSTRAR JSON MEDIANTE UN ALERT
			//  Toast.makeText(MapView.this, tempShopsResult,Toast.LENGTH_LONG).show();
		  }else{
			//MOSTRAR ERROR
			  Toast.makeText(MapView.this,"No hay tiendas en tu zona.", Toast.LENGTH_SHORT).show();
			  
		  }
	      	move = 0;
		}else{
			move++;
			if(move>=CONTROL_MOVE){
				move = 0;
			}
		}
	}
}